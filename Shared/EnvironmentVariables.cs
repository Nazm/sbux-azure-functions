﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared
{
    public class EnvironmentVariables
    {
        //public static string AzureServiceBusConnectionString = Environment.GetEnvironmentVariable("martech-digitalcontent_SERVICEBUS");
        public static string AzureKeyVaultDns = Environment.GetEnvironmentVariable("SBUX_KeyVault");
        public static string AzureKeyVaulLookintoDir = Environment.GetEnvironmentVariable("KeyVault_lookinto_dir");
        public static string ServiceBusQueueNameForAkamai = Environment.GetEnvironmentVariable("Keyvault-digitalcontent-deployment-akamai-dev-send-name");
        public static string ServiceBusQueueNameForFolio = Environment.GetEnvironmentVariable("Keyvault-digitalcontent-deployment-folio-dev-send-name");
        public static string ServiceBusQueueNameForPartnetSend = Environment.GetEnvironmentVariable("Keyvault-digitalcontent-deployment-partner-dev-send-name");
        public static string AkamaiPurgingEnvironment = Environment.GetEnvironmentVariable("Purging-environment");
        public static string AkamaiAccessTokenName = Environment.GetEnvironmentVariable("access_token_name");
        public static string AkamaiClientSecretName = Environment.GetEnvironmentVariable("client_secret_name");
        public static string AkamaiClientTokenName = Environment.GetEnvironmentVariable("client_token_name");
        public static string AkamaiFastPurgeEndpointName = Environment.GetEnvironmentVariable("host_name");
        public static string PartnerElasticSearchIndexingUrl = Environment.GetEnvironmentVariable("partner_elastic_url_name");

        public static string AkamaiApiExtension = Environment.GetEnvironmentVariable("akamai-api-url-extension");
        public static string AkamaiPurgingReqMethod = Environment.GetEnvironmentVariable("akamai-purging-req-method");
        public static string AkamaiPurgingReqContentType = Environment.GetEnvironmentVariable("akamai-purging-req-content-type");
        public static string ObjectToPurge = Environment.GetEnvironmentVariable("object-to-purge");
        public static string SendToElastic = Environment.GetEnvironmentVariable("item-to-send-to-partner-for-indexing");
    }
}
