﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class Publication
    {
        
        public List<String> baseURIs { get; set; }

        public String key { get; set; }

        public String title { get; set; }

        public String id { get; set; }
    }
}
