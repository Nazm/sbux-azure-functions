﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class DeploymentNotification
    {
      
        public String action { get; set; }
        public String transactionId { get; set; }
        public Publication publication { get; set; }
        public List<Binary> binaries { get; set; }
        public List<Schema> contentTypes { get; set; }
        public List<ComponentPresentation> componentPresentations { get; set; }
        public List<Page> pages { get; set; }
        public List<Keyword> taxonomies { get; set; }

    }
}
