﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class Taxonomy : Keyword
    {
        public bool usedForIdentification { get; set; }
    }
}
