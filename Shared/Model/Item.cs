﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class Item
    {
        public String id { get; set; }

        public String title { get; set; }

        public String url { get; set; }

        public String modified { get; set; }

        public String version { get; set; }
    }
}
