﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class Page : ItemWithMetadataAndKeywords
    {
        public String path { get; set; }
        public String pageTemplate { get; set; }
        public List<ComponentPresentation> componentPresentations { get; set; }
    }
}
