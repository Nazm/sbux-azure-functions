using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Akamai.EdgeGrid.Auth;
using System.Net;
using System.Text;
using Starbucks.Martech.DigitalContent.Deployment.Shared;
using System.Net.Http;

namespace Starbucks.Martech.DigitalContent.Deployment.Akamai
{
    class AkamaiPurgeByInvalidate
    {
        public String client_token { get; set; }
        public String access_token { get; set; }
        public String client_secret { get; set; }
        public String api_url { get; set; }


        public AkamaiPurgeByInvalidate(AkamaiSecretsMapping secret)
        {
            client_token = secret.ClientToken;
            access_token = secret.AccessToken;
            client_secret = secret.ClientSecret;
            api_url = secret.Host;
        }


        public string Invalidate(AkamaiPurgeObject purgeObject)
        {

            ClientCredential credentials = new ClientCredential(client_token, access_token, client_secret);
            EdgeGridV1Signer signer = new EdgeGridV1Signer(null, 100000);

            Uri uri = new Uri(api_url + EnvironmentVariables.AkamaiApiExtension + EnvironmentVariables.AkamaiPurgingEnvironment);
          
            WebRequest request = WebRequest.Create(uri);
            request.Method = EnvironmentVariables.AkamaiPurgingReqMethod;
            request.ContentType = EnvironmentVariables.AkamaiPurgingReqContentType;
            ServicePointManager.Expect100Continue = false;

            byte[] byteArray = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(purgeObject));
            MemoryStream uploadStream = new MemoryStream(byteArray);

            request = signer.Sign(request, credentials, uploadStream);

            if (uploadStream == null)
                request.ContentLength = 0;
            else if (uploadStream.CanSeek)
                request.ContentLength = uploadStream.Length;
            else if (request is HttpWebRequest)
                ((HttpWebRequest)request).SendChunked = true;

            if (uploadStream != null)
            {
                if (request is HttpWebRequest)
                    ((HttpWebRequest)request).AllowWriteStreamBuffering = false;

                using (Stream requestStream = request.GetRequestStream())
                using (uploadStream)
                    uploadStream.CopyTo(requestStream, 1024 * 1024);
            }

            try
            {
                var response = (WebResponse)request.GetResponse();
                HttpWebResponse httpResponse = (HttpWebResponse)response;
                Console.WriteLine("Status code: {0}", httpResponse.StatusCode);
                using (Stream data = response.GetResponseStream())
                using (var reader = new StreamReader(data))
                {
                    string text = reader.ReadToEnd();
                    Console.WriteLine(text);
                    return text;
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        Console.WriteLine(text);
                        return text;
                    }
                }
            }


        }
    }
}
