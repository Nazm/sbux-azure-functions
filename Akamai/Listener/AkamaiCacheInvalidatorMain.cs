using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Starbucks.Martech.DigitalContent.Deployment.Shared;
using Newtonsoft.Json.Linq;
using Starbucks.Martech.DigitalContent.Deployment.Shared.Model;
using System.Collections.Generic;

namespace Starbucks.Martech.DigitalContent.Deployment.Akamai
{
    public static class AkamaiCacheInvalidatorMain
    {
        

        [FunctionName("AkamaiCacheInvalidatorMain")]
        public static async Task Run([ServiceBusTrigger("digitalcontent-deployment-akamai-dev", Connection = "Keyvault_digitalcontent-deployment-akamai-dev-listen-connstring")]string AkamaiQueueItem, ILogger log)
        {
            log.LogInformation("Started processing a request to invalidate Akamai Cache.");

            DeploymentNotification notification = JsonConvert.DeserializeObject<DeploymentNotification>(AkamaiQueueItem);
            
            List<String> objects = new List<string>();
            objects.Add(EnvironmentVariables.ObjectToPurge);

            AzureKeyVaultService service = new AzureKeyVaultService();

            for (int temp=0; temp < notification.publication.baseURIs.Count; temp++) {

                AkamaiPurgeObject purgeObject = new AkamaiPurgeObject(objects, notification.publication.baseURIs[temp]);
            
                AkamaiPurgeByInvalidate invalidator = new AkamaiPurgeByInvalidate(await service.GetAkamaiSecretValues());
                string res = JsonConvert.SerializeObject(invalidator.Invalidate(purgeObject));

                log.LogInformation($"Akamai Cache invalidation response: {res}");
            }
        }
    }
}

/*{
    "objects": [
        "/"
    ],
    "hostname": "testsdlgreen.starbucks.com"
}*/
