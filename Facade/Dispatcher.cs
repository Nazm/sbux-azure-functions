using System;
using System.Net.Http;
using System.Text;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Starbucks.Martech.DigitalContent.Deployment.Shared;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Starbucks.Martech.DigitalContent.Deployment.Shared.Model;

namespace Starbucks.Martech.DigitalContent.Deployment.Trigger
{
    public static class Dispatcher
    {
        
        [FunctionName("Dispatcher")]
        public static async Task RunAsync([ServiceBusTrigger("digitalcontent-deployment-dev", Connection = "digitalcontent-deployment-dev-listen-connection-string" )]
                string QueueItem, ILogger log)
        {

            AzureKeyVaultService azureKeyVaultService = new AzureKeyVaultService();
          
            log.LogInformation(Environment.NewLine + $"RECEIVED MESSAGE: {QueueItem}" + Environment.NewLine);

            //MAPPING THE JSON INTO MODEL
            DeploymentNotification notification = JsonConvert.DeserializeObject<DeploymentNotification>(QueueItem);

            string publicationKey = notification?.publication?.key ?? string.Empty;
            bool isPartner = publicationKey.StartsWith("partner/");
            bool isFolio = publicationKey.StartsWith("www/us/") || publicationKey.StartsWith("www/ca/");
            bool invalidateAkamai = true;


            try
            {
                //JObject json = JObject.Parse(QueueItem);
                
                if (isPartner)
                {

                    
                    string partnerQueue = EnvironmentVariables.ServiceBusQueueNameForPartnetSend;
                    //log.LogInformation($"***ue: {partnerQueue}");
                    List<string> queueNameandConnString = await azureKeyVaultService._GetConnectionString(partnerQueue);
                    //log.LogInformation($"***ue: {queueNameandConnString[1]}");
                    await SendMessagesAsync(QueueItem, queueNameandConnString[1], queueNameandConnString[0], log);

                }
                if (isFolio)
                {

                    string folioQueue = EnvironmentVariables.ServiceBusQueueNameForFolio;
                    await SendMessagesAsync(QueueItem, folioQueue, await azureKeyVaultService.GetConnectionString(folioQueue), log);

                }
                if (invalidateAkamai)
                {
                    string akamaiQueue = EnvironmentVariables.ServiceBusQueueNameForAkamai;
                    List<string> queueNameandConnString = await azureKeyVaultService._GetConnectionString(akamaiQueue);
                    await SendMessagesAsync(QueueItem, queueNameandConnString[1], queueNameandConnString[0], log);
                }

            }
            catch (Exception e)
            {
                log.LogError($"_Exception in parsing incomming string from Deployer Extension: {e}");

            }

        }


        static async Task SendMessagesAsync(string MessagesToForward, string queueName, string connectionString, ILogger log)
        {

            IQueueClient queueClient = new QueueClient(connectionString, queueName);
            var message = new Message(Encoding.UTF8.GetBytes(MessagesToForward));
            try
            {
                await queueClient.SendAsync(message);
                log.LogInformation($"***Message Forwarded to Queue: {queueName}" + Environment.NewLine + /*$"Message: { MessagesToForward}" +*/ Environment.NewLine);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"{DateTime.Now} :: Exception: {exception.Message}");
            }
        }

        
    }
}
